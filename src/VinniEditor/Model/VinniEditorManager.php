<?php

namespace VinniEditor\Model;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class VinniEditorManager implements ServiceLocatorAwareInterface
{
    protected $renderer;
    protected $partialPath;
    protected $contentEntity;
    protected $relationEntity;
    protected $contextEntity;
    protected $serviceLocator;
    protected $data;
    protected $editable = true;

    /**
     * try to see if there is a matching data
     * that conforms with the current controller/action
     *  if it exists return it, otherwise only keep cache from the page
     */
    protected function getScoppedContent()
    {
        $routeMatch = $this->getServiceLocator()->get('Application')->getMvcEvent()->getRouteMatch();
        $currentControllerName = $routeMatch->getParam('controller');
        $this->data['controllerName'] = $currentControllerName;

        $currentViewName = $routeMatch->getParam('action');
        $this->data['actionName'] = $currentViewName;

        $where = array('controller'=>$currentControllerName, 'action' => $currentViewName);

        $context = $this->contextEntity->findBy($where);

        if (empty($context)) {
           return null;
        }

        $context = end($context);

        $relations =  $this->relationEntity->findBy(array('contextId'=>$context->getId()));

        if (empty($relations)) {
            return null;
        }

        $scoppedContent = array();
        foreach ($relations as $relation) {

            $content = $this->contentEntity->findById($relation->getContentId());
            $content = reset($content);

            if (!empty($this->data['identifier']) && $content->getIdentifier() == $this->data['identifier']) {
                $result = $content;
            }

            $scoppedContent[] = $content;
        }

        return $result;
    }

    protected function getData($key)
    {

        $content = $this->getScoppedContent();

        if (!empty($content)) {

            $getFunc = 'get'.ucfirst($key);

            $result = $content->$getFunc();

            if (!empty($result)) {
                $this->data[$key] = $result;
            }
        }

        return $this->data[$key];
    }

    public function render()
    {
        $data['title'] = $this->getData('title');
        $data['content'] = $this->getData('content');
        $data['editable'] = $this->getEditable('editable');

        //double due to javascript escapping
        $data['controllerName'] = str_replace('\\', '.', $this->data['controllerName']);
        $data['actionName'] = $this->data['actionName'];
        $data['identifier'] = $this->data['identifier'];

        $this->getRenderer()->render($this->getPartialPath(), $data);
    }

    public function __get($key)
    {
        if (property_exists($this, $key)) {
            return $this->$key;
        } else {
            return $this->data[$key];
        }
    }

    public function __set($key, $value)
    {
        if (property_exists($this, $key)) {
            $this->$key = $value;
        } else {
            $this->data[$key] = $value;
        }

        return $this;
    }

    public function __call($method, array $args)
    {

        $attrName = strtolower(substr($method, 3));
        $action = substr($method, 0, 3);

        if ($action == 'get') {
            return $this->__get($attrName);

        } elseif ($action == 'set') {

            $value = reset($args);

            return $this->__set($attrName, $value);
        }

        throw new \Exception('Não encontrou funcionalidade implementada em '.get_called_class().' para a chamada: '.$method);
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;

        return $this;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * getter de ContentEntity
     *
     * @return ContentEntity
     */
    public function getContentEntity()
    {
        return $this->contentEntity;
    }

    /**
     * setter de ContentEntity
     *
     * @param int $contentEntity
     *
     * @return $this retorna o próprio objeto
     */
    public function setContentEntity($contentEntity)
    {
        $this->contentEntity = $contentEntity;

        return $this;
    }

    /**
     * getter de ContextEntity
     *
     * @return ContextEntity
     */
    public function getContextEntity()
    {
        return $this->contextEntity;
    }

    /**
     * setter de ContextEntity
     *
     * @param int $contextEntity
     *
     * @return $this retorna o próprio objeto
     */
    public function setContextEntity($contextEntity)
    {
        $this->contextEntity = $contextEntity;

        return $this;
    }

    /**
     * getter de Renderer
     *
     * @return Renderer
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * setter de Renderer
     *
     * @param int $renderer
     *
     * @return $this retorna o próprio objeto
     */
    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;

        return $this;
    }

    /**
     * getter de PartialPath
     *
     * @return PartialPath
     */
    public function getPartialPath()
    {
        return $this->partialPath;
    }

    /**
     * setter de PartialPath
     *
     * @param int $partialPath
     *
     * @return $this retorna o próprio objeto
     */
    public function setPartialPath($partialPath)
    {
        $this->partialPath = $partialPath;

        return $this;
    }

    /**
     * getter de RelationEntity
     *
     * @return RelationEntity
     */
    public function getRelationEntity()
    {
        return $this->relationEntity;
    }

    /**
     * setter de RelationEntity
     *
     * @param int $relationEntity
     *
     * @return $this retorna o próprio objeto
     */
    public function setRelationEntity($relationEntity)
    {
        $this->relationEntity = $relationEntity;

        return $this;
    }

    /**
     * getter de Editable
     *
     * @return Editable
     */
    public function getEditable()
    {
        //inline setted has priorities
        if (isset($this->data['editable'])) {
            return $this->data['editable'];
        }

        return $this->editable;
    }

    /**
     * setter de Editable
     *
     * @param int $editable
     *
     * @return $this retorna o próprio objeto
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;

        return $this;
    }
}
