<?php
namespace VinniEditor\View\Helper;

use Zend\View\Helper\AbstractHelper;

class VinniEditor extends AbstractHelper
{

    public function __invoke()
    {
        $this->getView()->inlineScript()->offsetSetFile(100, '/vinni-editor/js/script.js');

        return $this->getView()->getHelperPluginManager()->getServiceLocator()->get('VinniEditorManager')->setRenderer($this);
    }

    public function render($partialName, $scoppedData)
    {
        echo $this->getView()->partial($partialName, $scoppedData);
    }
}
