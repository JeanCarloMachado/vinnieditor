<?php

namespace VinniEditor\Controller;

use Zend\Mvc\Controller\AbstractActionController;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

class IndexController extends AbstractActionController
{

    /**
     * recieve requests to create or update contents
     */
    public function createOrUpdateAction()
    {
        $controllerName = $this->params('controllerName');
        $controllerName = str_replace('.', '\\', $controllerName);

        $actionName = $this->params('actionName');
        $type = $this->params('type');
        $identifier = $this->params('identifier');
        $text = $this->params('text');

        $entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $context = $entityManager->getRepository('VinniEditor\Entity\Context');
        $content = $entityManager->getRepository('VinniEditor\Entity\Content');
        $contentContextRelation = $entityManager->getRepository('VinniEditor\Entity\ContentContext');

        $whereContext = array('controller'=>$controllerName, 'action'=>$actionName);
        $whereContent = array('manualIdentifier'=>$identifier, 'status' => 1);

        if ($type == 'title') {
            $whereContent['title'] = $text;
        } else {
            $whereContent['content'] = $text;
        }

        $hydrator = new DoctrineObject($entityManager);

        $contextResult = $context->findBy($whereContext);

        if (empty($contextResult)) {
            //creates a entity
            $contextResult = new \VinniEditor\Entity\Context;
            $hydrator->hydrate($whereContext, $contextResult);

            $entityManager->persist($contextResult);
            $entityManager->flush();

        } else {
            $contextResult = reset($contextResult);
        }

        //see if there is a content related with the context reuslt
        $contentContextResultSet = $contentContextRelation->findByContextId($contextResult->getId());

        $hasRelation = false;
        if (!empty($contentContextResultSet)) {

            //get elements that match the identifier and the id passed
            foreach ($contentContextResultSet as $relation) {
                $contentMatched = $content->findBy(array('id'=>$relation->getContentId(), 'manualIdentifier' => $identifier));

                if (!empty($contentMatched)) {
                    $contentMatched = reset($contentMatched);
                    $hasRelation = true;
                    break;
                }
            }
        }

        //creates a new content
        if (empty($contentMatched)) {

            $contentMatched = new \VinniEditor\Entity\Content;
        }

        $hydrator->hydrate($whereContent, $contentMatched);

        $entityManager->persist($contentMatched);
        $entityManager->flush();

        //if has no relation creates one
        if (!$hasRelation) {

            $whereContentContext = array('contentId'=>$contentMatched->getId(), 'contextId' => $contextResult->getId());
            $contentContextResult = new \VinniEditor\Entity\ContentContext;
            $hydrator->hydrate($whereContentContext, $contentContextResult);

            $entityManager->persist($contentContextResult);
            $entityManager->flush();

        }
                echo json_encode(array('message'=>'Everything worked fine!'));

        return $this->response;
    }
}
