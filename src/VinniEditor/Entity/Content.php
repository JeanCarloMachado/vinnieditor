<?php

namespace VinniEditor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VinnieditorContent
 *
 * @ORM\Table(name="vinnieditor_content")
 * @ORM\Entity
 */
class Content
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="manual_identifier", type="string", length=45, nullable=false)
     */
    private $manualIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="help_content", type="text", nullable=true)
     */
    private $helpContent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set manualIdentifier
     *
     * @param  string  $manualIdentifier
     * @return Content
     */
    public function setManualIdentifier($manualIdentifier)
    {
        $this->manualIdentifier = $manualIdentifier;

        return $this;
    }

    public function getIdentifier()
    {
        return $this->getManualIdentifier();
    }

    /**
     * Get manualIdentifier
     *
     * @return string
     */
    public function getManualIdentifier()
    {
        return $this->manualIdentifier;
    }

    /**
     * Set title
     *
     * @param  string  $title
     * @return Content
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param  string  $content
     * @return Content
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set helpContent
     *
     * @param  string  $helpContent
     * @return Content
     */
    public function setHelpContent($helpContent)
    {
        $this->helpContent = $helpContent;

        return $this;
    }

    /**
     * Get helpContent
     *
     * @return string
     */
    public function getHelpContent()
    {
        return $this->helpContent;
    }

    /**
     * Set status
     *
     * @param  boolean $status
     * @return Content
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
}
