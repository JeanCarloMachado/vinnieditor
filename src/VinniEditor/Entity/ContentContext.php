<?php

namespace VinniEditor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContentContext
 *
 * @ORM\Table(name="vinnieditor_content_context")
 * @ORM\Entity
 */
class ContentContext
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="content_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $contentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="context_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $contextId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contentId
     *
     * @param  integer        $contentId
     * @return ContentContext
     */
    public function setContentId($contentId)
    {
        $this->contentId = $contentId;

        return $this;
    }

    /**
     * Get contentId
     *
     * @return integer
     */
    public function getContentId()
    {
        return $this->contentId;
    }

    /**
     * Set contextId
     *
     * @param  integer        $contextId
     * @return ContentContext
     */
    public function setContextId($contextId)
    {
        $this->contextId = $contextId;

        return $this;
    }

    /**
     * Get contextId
     *
     * @return integer
     */
    public function getContextId()
    {
        return $this->contextId;
    }
}
