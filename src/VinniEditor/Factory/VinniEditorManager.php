<?php

namespace VinniEditor\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use VinniEditor\Model\VinniEditorManager as RealVinniEditorManager;

class VinniEditorManager implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        $em = new RealVinniEditorManager;
        $em->setContentEntity($entityManager->getRepository('\VinniEditor\Entity\Content'));
        $em->setContextEntity($entityManager->getRepository('\VinniEditor\Entity\Context'));
        $em->setRelationEntity($entityManager->getRepository('\VinniEditor\Entity\ContentContext'));

        $partialPath = $serviceLocator->get('Config');
        $partialPath = $partialPath['vinni_editor']['partial'];

        $em->setPartialPath($partialPath);

        return $em;
    }
}
