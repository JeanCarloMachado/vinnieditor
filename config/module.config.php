<?php

namespace VinniEditor;

return array(
    'vinni_editor' => array(
        'partial' => '/vinni-editor/default-layout.phtml',
    ),

    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __DIR__. '/../public',
            ),
        ),

        'caching' => array(
            'default' => array(
                'cache'     => 'Assetic\\Cache\\FilesystemCache',
                'options' => array(
                    'dir' => './public', // path/to/cache
                ),
            ),
        )
    ),

    'view_helpers' => array(
        'invokables' => array(
            'vinniEditor' => 'VinniEditor\View\Helper\VinniEditor'
        )
    ),

    'service_manager' => array(
        'factories' => array(
            'VinniEditorManager' => 'VinniEditor\Factory\VinniEditorManager'
        )
    ),

     'view_manager' => array(

        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view',
        ),
    ),

    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                                 __DIR__ . '/../src/' . __NAMESPACE__ . '/Entity',
                                 __DIR__ . '/../src/' . __NAMESPACE__ . '/Repository'
                                 )
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),

    'controllers' => array(
        'invokables' => array(
          'VinniEditor\Controller\Index' => 'VinniEditor\Controller\IndexController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'create-or-update' => array(
                 'type' => 'Segment',
                 'priority' => 666,
                 'options' => array(
                    'route'    => '/vinni-editor/create-or-update/:controllerName/:actionName/:type/:identifier/:text',
                    'constraints' => array(
                        'controllerName'     => '(.*)',
                        'actionName' => '[a-zA-Z]*',
                        'type' => '[a-zA-Z]*',
                        'identifier' => '[a-zA-Z]*',
                        'text' => '(.*)',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'VinniEditor\Controller',
                        'controller' => 'Index',
                        'action' => 'createOrUpdate'
                    ),
                ),
            ),

        )
    ),

);
