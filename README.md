VinniEditor
===========

VinniEditor is a inline editor of content. With VinniEditor you can simply double click to alter a text in your html and persist it to all future executions of that page. It's initial purpose was to provide a easy way of content editors to change session pages descriptions, but, actually, it can be used in anywhere a editable text is necessary.

The simplest workflow of VinniEditor is: once a entry is registered - see usage - you could double click the text element and a input will wrap it. On that input you can edit the content and, when that input lose focus, it will became a text again that will automatically be persisted on the database so, when you refresh that page, the new content will be rendered instead of the old one.


Prerequisites
-------------

This module assumes you have doctrine configured with a database connection. Since you have a connection you could map the entities available in this module using the default doctrine process, as described bellow.

`
./vendor/bin/doctrine-module orm:schema-tool:create
`


Installation
------------


#### Installation steps

  1. Add the following line to your composer.json:

     ```json
      "jean-carlo-machado/vinni-editor": "dev-master"
     ```
  2. Run `php composer.phar update`
  3. Run the installation process
    `php vendor/jean-carlo-machado/vinni-editor/bin/install.php`

#### Post installation

1. Enabling it in your `application.config.php`file.

    ```php
    <?php
    return array(
        'modules' => array(
            // ...
            'VinniEditor',
        ),
        // ...
    );
    ```
    
Usage
-----
VinniEditor already register helpers to you and, in it's default behaviour, you can use it by simply calling on a view:


```php

$this->vinniEditor()->setIdentifier('moduleDescription')
  ->setTitle('Default Title Hardcoded')
  ->setContent('Default Content Hardcoded')
  ->render();
```



Note that setTitle and setContent are optional default values - overwrited when another text came from the database. The only real requirements are setIdentifier and render that are used to locate the text on the page and render the html template, respectively.

#License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.

#Contributing

To contribute you can simple make a pull request on this repository. 
