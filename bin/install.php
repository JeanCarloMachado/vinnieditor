<?php

function recurs_copy($src,$dst)
{
    $dir = opendir($src);
    @mkdir($dst);
    while (false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) ) {
                recurs_copy($src . '/' . $file,$dst . '/' . $file);
            } else {
                copy($src . '/' . $file,$dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

$dir = __DIR__;
echo PHP_EOL."Copying the public folder of vinni-editor on the public folder of the application".PHP_EOL;

$destination = is_dir($dir.'/../../../public') ? $dir.'/../../../public/vinni-editor' : $dir.'/../../../../public/vinni-editor';
$origin = $dir.'/../public/vinni-editor';

if (file_exists($destination)) {
    rmdir($destination);
}

recurs_copy($origin, $destination);
