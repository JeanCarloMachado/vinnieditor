var elements = document.getElementsByClassName('vinni-editable');
var editing = false;

function ajaxRequest(){
 var activexmodes=["Msxml2.XMLHTTP", "Microsoft.XMLHTTP"] //activeX versions to check for in IE
 if (window.ActiveXObject){ //Test for support for ActiveXObject in IE first (as XMLHttpRequest in IE7 is broken)
  for (var i=0; i<activexmodes.length; i++){
   try{
    return new ActiveXObject(activexmodes[i])
   }
   catch(e){
    //suppress error
   }
  }
 }
 else if (window.XMLHttpRequest) // if Mozilla, Safari etc
  return new XMLHttpRequest()
 else
  return false;
}

index = 0;

while(index < elements.length) {

    elements[index].ondblclick = function(){
        if (!editing) {

            if (this.innerHTML.length < 50) {

              this.innerHTML = '<input type="text" value="'+this.innerHTML+'"/>';
            }  else {
              this.innerHTML = '<textarea>'+this.innerHTML+'</textarea>';
            }

            this.firstChild.onblur = function(){
                if (editing) {
                    content = this.value;
                    identifier = this.parentNode.getAttribute('data-identifier');
                    type = this.parentNode.hasAttribute('data-type') ? this.parentNode.getAttribute('data-type') : '';

                    this.parentNode.innerHTML = this.value;
                    editing = false;

                    //send ajax with da data
                    controllerName = currentControllerName;
                    actionName = currentActionName;

                    var resquest = new ajaxRequest();

                    resquest.open('POST','/vinni-editor/create-or-update/'+controllerName+'/'+actionName+'/'+type+'/'+identifier+'/'+content,true);
                    resquest.send();

                }
            };

            editing = true;
        }
    };
    index++;
}
